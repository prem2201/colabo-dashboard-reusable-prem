import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import React from "react";
import Buket from "../../components/buckets/bucket";
import Bukect from "../../components/buckets/bucket2";
const useStyles = makeStyles((theme) => ({
  root: {
    overflow: "hidden",
    backgroundColor: "#F2F4F6",
  },
  border: {
    borderTop: "1px solid #E4EAEE",
  },
  paper: {
    borderRadius: 10,
  },
  rightbar: {
    marginTop: -10,
  },
}));

export const Home = (props) => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={12} sm={12} md={12} lg={3}>
          <Box
            width="100%"
            height="100%"
            borderTop={1}
            className={classes.border}
            backgroundColor="white"
          >
            <div style={{ paddingLeft: 6, paddingRight: 6 }}>
              <Buket />
            </div>
          </Box>
        </Grid>
        <Grid item xs={12} sm={12} md={12} lg={9}>
          <Paper p={1} className={classes.paper}>
            <Box m={1}>
              <Bukect />
            </Box>
          </Paper>
        </Grid>
      </Grid>
    </div>
  );
};

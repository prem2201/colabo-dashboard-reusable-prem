import { CssBaseline } from "@material-ui/core";
import React from "react";
import AppAlert from "./App.alert";
import AppAuth from "./App.auth";
import AppBackdrop from "./App.backdrop";
import AppDialog from "./App.dialog";
import AppDrawer from "./App.drawer";
import AppErrorBoundary from "./App.errorBoundry";
import ApolloClient from "./App.gqlclient";
import AppTheme from "./App.theme";
import RouterApp from "./router";

const App = () => {
  return (
    <ApolloClient>
      <AppErrorBoundary>
        <AppAuth>
          <AppTheme>
            <CssBaseline />
            <AppAlert>
              <AppDialog>
                <AppDrawer>
                  <AppBackdrop>
                    <RouterApp />
                  </AppBackdrop>
                </AppDrawer>
              </AppDialog>
            </AppAlert>
          </AppTheme>
        </AppAuth>
      </AppErrorBoundary>
    </ApolloClient>
  );
};
export default App;

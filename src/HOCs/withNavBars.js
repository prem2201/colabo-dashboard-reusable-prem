import { makeStyles } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import React from "react";
import { SideNavBar, TopNavBar } from "../components";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    height: "87%",
    backgroundColor: "#F2F4F6",
  },
  content: {
    width: "100%",
    //height: "calc(100% - 64px)",
    height: "85vh",
    overflow: "auto",
    [theme.breakpoints.up("sm")]: {
      paddingLeft: 60,
    },
    [theme.breakpoints.down("sm")]: {
      paddingLeft: 0,
    },
  },
  topNavbar: {
    width: "100%",
  },
  sideNavbar: {
    [theme.breakpoints.down("sm")]: {
      display: "none",
    },
  },
}));

const withNavBars = (Component) => (props) => {
  const classes = useStyles({ props });

  return (
    <div className={classes.root}>
      <Box display="flex">
        <Box>
          <div className={classes.sideNavbar}>
            <SideNavBar />
          </div>
        </Box>
      </Box>
      <Grid container>
        <Grid item xs={12}>
          <div className={classes.topNavbar}>
            <TopNavBar />
          </div>
        </Grid>
      </Grid>

      <div className={classes.content}>
        <Component {...props}>{props.children}</Component>
      </div>
    </div>
  );
};

export default withNavBars;

import { Drawer } from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/InputBase";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import React from "react";
import { AiOutlineGift } from "react-icons/ai";
import { BiCaretDown } from "react-icons/bi";
import { RiNotificationLine } from "react-icons/ri";
import { VscCircleFilled } from "react-icons/vsc";
import { SideNavBar } from "..";

const useStyles = makeStyles((theme) => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    backgroundColor: "white",
    borderBottom: "1px solid #E4EAEE",
  },
  title: {
    color: "black",
    marginTop: 14,
    float: "left",
  },

  menuIcon: {
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
    marginLeft: -13,
    marginTop: 13,
  },
  search: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
  },
  inputs: {
    padding: 3,
  },
  circle: {
    width: "1.6rem",
    height: "1.6rem",
    padding: 5,
    marginTop: 10,
    marginRight: 13,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    padding: 8,
    textTransform: "capitalize",
    fontSize: 14,
  },
  profile: {
    width: "3rem",
    height: "3rem",
    borderRadius: 16,
    marginTop: 7,
    marginLeft: 10,
  },
  icons: {
    color: "black",
    fontSize: 25,
    marginTop: 6,
  },
  icon: {
    fontSize: 10,
  },
  note: {
    color: "#4B9CB4",
    fontSize: 12,
    position: "absolute",
    top: 7,
    left: 15,
    backgroundColor: "white",
  },
  border: {
    borderLeft: "1px solid #E4EAEE",
    padding: 5,
    height: 25,
    marginTop: 9,
  },
}));
const TopbarTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      textAlign: "center",
    },
  },
});

export const TopNavBar = (props) => {
  const classes = useStyles();

  const [state, setState] = React.useState({
    openSideNavBar: false,
  });

  const toogleSideNavBar = () => {
    setState({
      ...state,
      openSideNavBar: !state.openSideNavBar,
    });
  };

  return (
    <ThemeProvider theme={TopbarTheme}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appBar}>
          <Toolbar>
            <Grid container>
              <Grid item xs={1}>
                <IconButton
                  className={classes.menuIcon}
                  onClick={toogleSideNavBar}
                >
                  <MenuIcon />
                </IconButton>
              </Grid>
              <Grid item xs={11}>
                <Grid container>
                  <Grid item xs={12} md={4} lg={4}>
                    <Box className={classes.titleContainer} m={1}>
                      <Typography
                        className={classes.title}
                        variant="h6"
                        noWrap
                        m={1}
                      >
                        Productivity Framework
                      </Typography>
                    </Box>
                  </Grid>
                  <Grid item xs={12} md={3} lg={3}>
                    <Box p={1} m={1}>
                      <Box bgcolor="secondary.main" className={classes.search}>
                        <FormControl>
                          <Input
                            placeholder="Search"
                            className={classes.inputs}
                            startAdornment={
                              <InputAdornment position="start">
                                <IconButton>
                                  <SearchIcon />
                                </IconButton>
                              </InputAdornment>
                            }
                          />
                        </FormControl>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={12} md={5} lg={5}>
                    <div style={{ width: "100%" }}>
                      <Box
                        m={1}
                        display="flex"
                        flexDirection="row"
                        justifyContent="flex-end"
                      >
                        <Box>
                          <Box borderRadius="50%" className={classes.circle}>
                            <img
                              height="30px"
                              width="30px"
                              src="https://image.flaticon.com/icons/png/128/1831/1831908.png"
                              alt="img"
                            />
                          </Box>
                        </Box>
                        <Box p={1}>
                          <Button
                            size="small"
                            className={classes.button}
                            endIcon={<BiCaretDown className={classes.icon} />}
                          >
                            Add&nbsp;New
                          </Button>
                        </Box>
                        <Box p={1}>
                          <div className={classes.border}></div>
                        </Box>
                        <Box m={1}>
                          <IconButton size="small">
                            <AiOutlineGift className={classes.icons} />
                          </IconButton>
                        </Box>

                        <Box m={1}>
                          <IconButton size="small">
                            <RiNotificationLine className={classes.icons} />
                            <span className={classes.note}>
                              <VscCircleFilled />
                            </span>
                          </IconButton>
                        </Box>
                        <Box>
                          <img
                            border="1"
                            className={classes.profile}
                            src="https://www.diethelmtravel.com/wp-content/uploads/2016/04/bill-gates-wealthiest-person-279x300.jpg"
                            alt="img"
                          />
                        </Box>
                      </Box>
                    </div>
                  </Grid>
                </Grid>
              </Grid>
            </Grid>

            <Drawer
              open={state.openSideNavBar}
              variant={"temporary"}
              anchor="left"
              onClose={toogleSideNavBar}
            >
              <div style={{ width: 80 }}>
                <SideNavBar isMobile={true} />
              </div>
            </Drawer>
          </Toolbar>
        </AppBar>
      </div>
    </ThemeProvider>
  );
};

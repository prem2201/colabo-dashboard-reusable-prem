import Box from "@material-ui/core/Box";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Paper from "@material-ui/core/Paper";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { BsBullseye } from "react-icons/bs";
import { CgFileDocument } from "react-icons/cg";
import { FiSettings } from "react-icons/fi";
import { GiSandsOfTime } from "react-icons/gi";
import { HiFire } from "react-icons/hi";
import { IoIosInfinite } from "react-icons/io";
const drawerWidth = 70;
const useStyles = makeStyles((theme) => ({
  root: {
    width: (props) => (props?.isMobile ? 240 : drawerWidth),
    position: "absolute",
  },
  drawer: {
    //height: (props) => (props?.isMobile ? `100vh` : `calc(100vh - 44px)`),
    height: "100vh",
    width: (props) => (props?.isMobile ? 260 : drawerWidth),
    backgroundColor: "#11174C",
  },

  icons: {
    fontSize: 23,
    color: "white",
    fontWeight: "bold",
  },
  topicon: {
    fontSize: 35,
    color: "#C74F7A",
    fontWeight: "bold",
  },
  bgicons: {
    backgroundColor: "#333069",
    borderRadius: 8,
    marginTop: 10,
    padding: 5,
  },
  bgiconss: {
    backgroundColor: "#63B9A8",
    borderRadius: 8,
    marginTop: 10,
    padding: 5,
  },
}));

export const SideNavBar = (props) => {
  const classes = useStyles(props);

  const outerTheme = createTheme({
    palette: {
      secondary: {
        main: "#fff",
      },
    },
    typography: {
      subtitle2: {
        fontSize: 10,
        textAlign: "center",
      },
    },
  });
  return (
    <ThemeProvider theme={outerTheme}>
      <div className={classes.root}>
        <Paper className={classes.drawer} square>
          <div className={classes.drawerContainer} style={{ height: "50%" }}>
            <List>
              <ListItem button>
                <ListItemIcon>
                  <IoIosInfinite className={classes.topicon} />
                </ListItemIcon>
              </ListItem>
            </List>

            <List>
              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgicons}>
                      <HiFire className={classes.icons} />
                    </div>
                  </ListItemIcon>
                </ListItem>
              }
              <Typography variant="subtitle2" color="secondary">
                Actions
              </Typography>
              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgicons}>
                      <BsBullseye className={classes.icons} />
                    </div>
                  </ListItemIcon>
                </ListItem>
              }
              <Typography variant="subtitle2" color="secondary">
                Goals
              </Typography>
              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgicons}>
                      <CgFileDocument className={classes.icons} />
                    </div>
                  </ListItemIcon>
                </ListItem>
              }
              <Typography variant="subtitle2" color="secondary">
                Docs
              </Typography>
              {
                <ListItem button>
                  <ListItemIcon>
                    <div className={classes.bgiconss}>
                      <GiSandsOfTime className={classes.icons} />
                    </div>
                  </ListItemIcon>
                </ListItem>
              }
              <Typography variant="subtitle2" color="secondary">
                Canvas
              </Typography>
            </List>
          </div>
          <Box
            style={{ height: "50%" }}
            display="flex"
            flexDirection="column"
            justifyContent="flex-end"
          >
            <Box>
              <ListItem button>
                <ListItemIcon>
                  <div className={classes.bgicons}>
                    <FiSettings className={classes.icons} />
                  </div>
                </ListItemIcon>
              </ListItem>
            </Box>
            <Box>
              <Typography variant="subtitle2" color="secondary">
                Settings
              </Typography>
            </Box>
          </Box>
        </Paper>
      </div>
    </ThemeProvider>
  );
};

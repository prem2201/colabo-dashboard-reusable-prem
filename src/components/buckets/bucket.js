import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import FormControl from "@material-ui/core/FormControl";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import Input from "@material-ui/core/InputBase";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import React from "react";
import { BiCaretDown } from "react-icons/bi";
import { BsFunnel } from "react-icons/bs";
import Card from "./card";
import Heading from "./heading";
const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
    backgroundColor: "white",
    padding: 5,
    height: "100%",
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
  },
  circle: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",
    marginLeft: 6,
  },
  color: {
    color: "white",
    marginTop: 2,
  },
  buket: {
    marginLeft: 15,
  },
  search: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
    marginTop: 7,
  },
  inputs: {
    padding: 3,
  },
  icons: {
    color: "black",
    fontSize: 24,
  },
  border: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    padding: 5,
    marginTop: 5,
    marginLeft: 4,
  },
  box: {
    width: "100%",
    padding: 15,
    marginTop: -20,
  },
  button: {
    border: "1px solid #E4EAEE",
    borderRadius: 8,
    paddingTop: 6,
    paddingBottom: 6,
    paddingRight: 10,
    paddingLeft: 10,
  },
}));
const BuketTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    h6: {
      fontSize: 15,
    },
    subtitle1: {
      fontSize: 12,
      fontWeight: "bold",
    },
  },
});

export default function CenteredGrid() {
  const classes = useStyles();

  return (
    <ThemeProvider theme={BuketTheme}>
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12}>
            <div style={{ padding: 6 }}>
              <Heading heading="My Bucket" number="16" />
            </div>
          </Grid>
        </Grid>
        {/* bucket */}
        <div className={classes.box}>
          <Grid container spacing={2}>
            <Grid item xs={12} md={6} lg={12}>
              <Grid container>
                <Grid item xs={10}>
                  <Box bgcolor="secondary.main" className={classes.search}>
                    <FormControl>
                      <Input
                        placeholder="Search"
                        className={classes.inputs}
                        startAdornment={
                          <InputAdornment position="start">
                            <IconButton size="small">
                              <SearchIcon className={classes.icons} />
                            </IconButton>
                          </InputAdornment>
                        }
                      />
                    </FormControl>
                  </Box>
                </Grid>
                <Grid item xs={2}>
                  <div className={classes.border}>
                    <IconButton size="small">
                      <BsFunnel className={classes.icons} />
                    </IconButton>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={6} lg={12}>
              <Grid container>
                <Grid item sm={4} md={3} lg={4}>
                  <Box>
                    <Button
                      size="small"
                      className={classes.button}
                      endIcon={<BiCaretDown className={classes.icon} />}
                    >
                      Today
                    </Button>
                  </Box>
                </Grid>
                <Grid item sm={4} md={3} lg={4}>
                  <Box>
                    <Button
                      size="small"
                      className={classes.button}
                      endIcon={<BiCaretDown className={classes.icon} />}
                    >
                      To&nbsp;Me
                    </Button>
                  </Box>
                </Grid>
                <Grid item sm={4} md={3} lg={4}>
                  <Box>
                    <Button
                      size="small"
                      className={classes.button}
                      endIcon={<BiCaretDown className={classes.icon} />}
                    >
                      open
                    </Button>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} md={6} lg={12}>
              <Card
                date="jun 29,2021"
                time="8.00pm"
                content="Completion of productivity framework  
                      design on or before july 7th,2021"
                pending="pending"
                percentage="35"
                starcolor="true"
                btn="true"
              />
            </Grid>
            <Grid item xs={12} md={6} lg={12}>
              <Card
                date="jun 29,2021"
                time="8.00pm"
                content="Coordinate with development team for
                assume module"
                pending="pending"
                percentage="70"
                starcolor="true"
                flag="false"
                btn="true"
                btncontent="true"
              />
            </Grid>
            <Grid item xs={12} md={6} lg={12}>
              <Card
                date="jun 29,2021"
                time="8.00pm"
                content="Coordinate with development team for
                assume module"
                pending="pending"
                percentage="70"
                starcolor="false"
                flag="false"
              />
            </Grid>
          </Grid>
        </div>
      </div>
    </ThemeProvider>
  );
}

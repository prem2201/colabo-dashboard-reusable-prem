import { Typography } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Tooltip from "@material-ui/core/Tooltip";
import React, { useEffect, useState } from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { AiFillStar, AiOutlineClockCircle } from "react-icons/ai";
import { BiCaretDown } from "react-icons/bi";
import { BsBullseye, BsStar } from "react-icons/bs";
import { CgNotes } from "react-icons/cg";
import { FiMoreVertical } from "react-icons/fi";
import { RiFlag2Fill } from "react-icons/ri";
const useStyles = makeStyles((theme) => ({
  root: {
    border: "1px solid #E4EAEE",
    borderRadius: 10,
    backgroundColor: "white",
    display: "block",
    padding: 0,
    margin: 0,
  },
  circle: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",

    marginTop: 10,
  },
  color: {
    color: "white",
  },
  buket: {
    marginLeft: 10,
  },

  icon: {
    fontSize: 13,
    color: "#E5513C",
  },
  icon1: {
    fontSize: 13,
    color: "#EF9B12",
  },
  border: {
    border: "1px solid #E4EAEE",
    padding: 6,
    borderRadius: 8,
  },
  box: {
    width: "90%",
    padding: 15,
  },

  over: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 4,
    textTransform: "capitalize",
    border: "1px solid #E5513C",
    backgroundColor: "#FCF1F0",
    color: "#E4513E",
  },
  left: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 5,
    textTransform: "capitalize",
    border: "1px solid #EF9B12",
    backgroundColor: "#FEF7EC",
    color: "#EFA978",
  },
  sideborder: {
    borderLeft: "1px solid #666666",
  },
  star: {
    fontSize: 18,
    color: "#EF9C14",
    marginTop: 2,
  },
  emptystar: {
    fontSize: 18,
    color: "#B4B4B4",
    marginTop: 2,
  },
  flag: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 3,
  },
  more: {
    fontSize: 18,
    marginTop: 2,
  },

  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
    marginLeft: 7,
  },
  btn: {
    border: "1px solid #E4EAEE",
    height: 24,
    float: "right",
    textTransform: "capitalize",
    fontSize: 11,
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginTop: 3,
    marginLeft: 3,

    width: 20,
  },
  cardcontent: {
    marginLeft: 8,
    padding: 4,
    marginTop: 1,
  },
  cardicon: {
    color: "#949494",
    fontSize: 16,
    marginTop: 4,
  },

  percent: {
    marginTop: -15,
    marginLeft: 22,
  },
  btnicon: {
    fontSize: 4,
  },
  dash: {
    borderTop: "1px dashed #989898",
    marginTop: 8,
  },
  typo: {
    marginTop: -12,
  },
  top: {
    marginTop: 20,
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 10,
      color: "#535353",
      whiteSpace: "nowrap",
    },
    subtitle1: {
      fontSize: 13,
      fontWeight: "600",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 10,
      fontWeight: "bold",
      textTransform: "lowercase",
    },
    h5: {
      fontSize: 11,
      marginTop: 3,
      marginLeft: -1,
    },
  },
});

export default function Cards(props) {
  const classes = useStyles();
  const [star, setstar] = useState(false);
  const [starcolor, setstarColor] = useState(true);
  const [flag, setFlag] = useState(true);
  const [btn, setBtn] = useState(false);
  const [btncontent, setBtncontent] = useState(true);
  const [bottom, setBottom] = useState(true);

  useEffect(() => {
    if (props.starcolor == "false") {
      setstarColor(false);
    } else {
      setstarColor(true);
    }
    if (props.star == "false") {
      setstar(true);
    } else {
      setstar(false);
    }
    if (props.flag == "false") {
      setFlag(false);
    } else {
      setFlag(true);
    }
    if (props.btn == "true") {
      setBtn(false);
    } else {
      setBtn(true);
    }
    if (props.btncontent == "true") {
      setBtncontent(false);
    } else {
      setBtncontent(true);
    }
    if (props.bottom == "true") {
      setBottom(false);
    } else {
      setBottom(true);
    }
  });

  return (
    <ThemeProvider theme={CardTheme}>
      <Box className={classes.root}>
        <Grid container>
          <Grid item xs={12} sm={12} md={6} lg={5}>
            <Box
              display="flex"
              flexDirection="row"
              m={2}
              fontSize={10}
              className={classes.top}
            >
              <Box>
                <span style={{ whiteSpace: "nowrap" }}>
                  {props.date}&nbsp;&nbsp;
                </span>
              </Box>
              <Box className={classes.sideborder}>
                <span>&nbsp;&nbsp;{props.time}</span>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={6} sm={6} md={3} lg={4}>
            <Box display="flex" flexDirection="row" m={1}>
              {flag ? (
                <IconButton size="small">
                  <Box
                    borderRadius="50%"
                    bgcolor="#FCF1F0"
                    className={classes.flag}
                  >
                    <RiFlag2Fill className={classes.icon} />
                  </Box>
                </IconButton>
              ) : (
                <IconButton size="small">
                  <Box
                    borderRadius="50%"
                    bgcolor="#FCF1F0"
                    className={classes.flag}
                  >
                    <RiFlag2Fill className={classes.icon1} />
                  </Box>
                </IconButton>
              )}

              <Box hidden={btn} marginLeft={1}>
                {btncontent ? (
                  <Button size="small" className={classes.over}>
                    overdue
                  </Button>
                ) : (
                  <Button size="small" className={classes.left}>
                    2 hours
                  </Button>
                )}
              </Box>
            </Box>
          </Grid>
          <Grid item xs={6} sm={6} md={3} lg={3}>
            <Box
              display="flex"
              justifyContent="flex-end"
              flexDirection="row"
              m={1}
            >
              <Box>
                <div hidden={star}>
                  {starcolor ? (
                    <IconButton size="small">
                      <AiFillStar className={classes.star} />
                    </IconButton>
                  ) : (
                    <IconButton size="small">
                      <BsStar className={classes.emptystar} />
                    </IconButton>
                  )}
                </div>
              </Box>
              <Box>
                <IconButton size="small">
                  <FiMoreVertical className={classes.more} />
                </IconButton>
              </Box>
            </Box>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12}>
            <Box
              className={classes.cardcontent}
              width={280}
              textAlign="left"
              fontSize={13}
              marginTop={-1}
            >
              <span>{props.content}</span>
            </Box>
            <Box display="flex" flexWrap="nowrap" marginTop={1}>
              <Box>
                <Button size="small" className={classes.margin}>
                  <Typography variant="body2">5157-Feature</Typography>
                </Button>
              </Box>
              <Box>
                <Button size="small" className={classes.margin}>
                  <Typography variant="body2">Goal</Typography>
                </Button>
              </Box>
              <Box>
                <Button size="small" className={classes.margin}>
                  <Typography variant="body2">#Colabo</Typography>
                </Button>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12}>
            <Box
              display="flex"
              flexWrap="nowrap"
              className={classes.cardcontent}
              p={1}
            >
              <Box>
                <Tooltip title="Goal(2)" arrow>
                  <IconButton size="small">
                    <BsBullseye className={classes.cardicon} />
                  </IconButton>
                </Tooltip>
              </Box>
              <Box>
                <Tooltip title="Tasks(2)" arrow>
                  <IconButton size="small">
                    <CgNotes className={classes.cardicon} />
                  </IconButton>
                </Tooltip>
              </Box>
              <Box>
                <div className={classes.progress}>
                  <CircularProgressbarWithChildren
                    value={props.percentage}
                    strokeWidth={15}
                    styles={{
                      path: {
                        stroke: `#77CF71`,
                        strokeLinecap: "butt",
                      },
                      trail: {
                        stroke: "#d6d6d6",
                        strokeLinecap: "butt",
                      },
                    }}
                  ></CircularProgressbarWithChildren>
                  <Typography variant="subtitle2" className={classes.percent}>
                    {props.percentage}%
                  </Typography>
                </div>
              </Box>

              <Box flexGrow={1}>
                <Button
                  size="small"
                  className={classes.btn}
                  endIcon={<BiCaretDown className={classes.btnicon} />}
                >
                  <span>{props.pending}</span>
                </Button>
              </Box>
            </Box>
          </Grid>
        </Grid>
        <div hidden={bottom}>
          <Grid container>
            <Grid item xs={12}>
              <div style={{ width: "100%" }} className={classes.dash}>
                <Box display="flex" m={1}>
                  <Box flexGrow={1}>
                    <Box display="flex" flexDirection="row">
                      <Box>
                        <Typography variant="subtitle2" noWrap>
                          schedule to jun 30,2021&nbsp;
                        </Typography>
                      </Box>
                      <Box className={classes.sideborder}>
                        <Typography variant="subtitle2">
                          &nbsp;8.00&nbsp;pm
                        </Typography>
                      </Box>
                    </Box>{" "}
                  </Box>
                  <Box>
                    <Box>
                      <AiOutlineClockCircle />
                    </Box>
                  </Box>
                </Box>
              </div>
            </Grid>
          </Grid>
        </div>
      </Box>
    </ThemeProvider>
  );
}

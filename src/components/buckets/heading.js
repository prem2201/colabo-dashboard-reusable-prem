import { Typography } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
const useStyles = makeStyles((theme) => ({
  avatar: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",
    fontSize: 12,
    fontWeight: 500,
    textAlign: "center",
  },
}));
const CardTheme = createTheme({
  typography: {
    subtitle1: {
      fontSize: 13,
      fontWeight: "bold",
      fontFamily: "adobe-clean, sans-serif",
    },
  },
});

export default function Cards(props) {
  const classes = useStyles();

  return (
    <ThemeProvider theme={CardTheme}>
      <div>
        <Grid container>
          <Grid item xs={12}>
            <Box display="flex" flexDirection="row">
              <Box marginLeft={1}>
                <Typography variant="subtitle1">{props.heading}</Typography>
              </Box>
              <Box marginLeft={1}>
                <Avatar className={classes.avatar}>{props.number}</Avatar>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
}

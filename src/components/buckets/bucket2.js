import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import { useHistory } from "react-router-dom";
import Card from "./card";
import Heading from "./heading";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    overflow: "hidden",
  },
  btngoal: {
    border: "none",
    backgroundColor: "transparent",
    cursor: "pointer",
  },
  circle: {
    backgroundColor: "black",
    width: "1.4rem",
    height: "1.4rem",
    marginLeft: 6,
  },
  color: {
    color: "white",
    marginTop: 2,
  },
  buket: {
    marginLeft: 15,
  },
  highlights: {
    backgroundColor: "#e8f0da",
    borderRadius: 10,
    border: "1px solid #F8FBF3",
    height: 694,
    marginTop: 6,

    marginLeft: 10,
  },
  height: {
    height: 337,
    marginTop: 6,
  },
}));

export default function Buket() {
  const history = useHistory();
  const classes = useStyles();
  const goTo = () => {
    history.push("/goals");
  };
  return (
    <div>
      <Grid container>
        <Grid item xs={12} sm={12} md={8} lg={8}>
          <Grid container spacing={1}>
            <Grid item xs={12} md={6}>
              <Box p={1} height="100%">
                <Box
                  p={1}
                  bgcolor="#fff4eb"
                  borderRadius={14}
                  className={classes.height}
                >
                  <div style={{ padding: 6 }}>
                    <Heading heading="Do First" number="02" />
                  </div>
                  <div className={classes.btngoal} onClick={goTo}>
                    <Card
                      date="jun 29,2021"
                      time="8.00pm"
                      content="Completion of productivity framework  
                            design on or before july 7th,2021"
                      pending="pending"
                      percentage="35"
                      starcolor="true"
                      btn="true"
                      star="false"
                    />
                  </div>
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} md={6}>
              <Box p={1} height="100%">
                <Box
                  p={1}
                  bgcolor="#ebedf5"
                  borderRadius={14}
                  className={classes.height}
                >
                  <div style={{ padding: 6 }}>
                    <Heading heading="Schedule" number="01" />
                  </div>

                  <Card
                    date="jun 29,2021"
                    time="8.00pm"
                    content="Coordinate with development team for
                    assume module"
                    pending="In development"
                    percentage="70"
                    starcolor="true"
                    flag="false"
                    btn="true"
                    btncontent="true"
                    bottom="true"
                  />
                </Box>
              </Box>
            </Grid>
          </Grid>
          <Grid container spacing={1}>
            <Grid item xs={12} md={6}>
              <Box p={1} height="100%">
                <Box
                  p={1}
                  bgcolor="#e4f5ef"
                  borderRadius={14}
                  className={classes.height}
                >
                  <div style={{ padding: 6 }}>
                    <Heading heading="Delegate" number="01" />
                  </div>

                  <Card
                    date="jun 29,2021"
                    time="8.00pm"
                    content="coordinate with development team for
                    assume module"
                    pending="pending"
                    percentage="70"
                    starcolor="true"
                    star="false"
                    flag="false"
                  />
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12} md={6}>
              <Box
                bgcolor="#FCF2F3"
                className={classes.height}
                borderRadius={15}
              >
                <div style={{ marginTop: 15, padding: 6 }}>
                  <Heading heading="Don't" number="0" />
                </div>
              </Box>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4}>
          <Box className={classes.highlights} p={1}>
            <div style={{ padding: 6 }}>
              <Heading heading="Highlights" number="02" />
            </div>

            <Card
              date="jun 29,2021"
              time="8.00pm"
              content="Collabrate with health circle DEV
              team fror UI feedbacks"
              pending="pending"
              percentage="15"
              starcolor="true"
              btn="true"
              star="false"
            />
            <br />
            <Card
              date="jun 29,2021"
              time="8.00pm"
              content="Forward TanyaCare Web design
              requirments to natha"
              pending="pending"
              percentage="100"
              starcolor="true"
              btn="false"
              star="false"
              flag="false"
              btncontent="true"
            />
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}

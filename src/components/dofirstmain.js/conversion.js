import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Divider from "@material-ui/core/Divider";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
import {
  AiOutlineBold,
  AiOutlineItalic,
  AiOutlineOrderedList,
  AiOutlineSend,
  AiOutlineSmile,
  AiOutlineUnderline,
  AiOutlineUnorderedList,
} from "react-icons/ai";
import { BiConversation } from "react-icons/bi";
import { BsPeopleCircle } from "react-icons/bs";
import { IoIosAttach } from "react-icons/io";
import Chat from "./chat";
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
    marginLeft: 6,
    padding: 14,
    borderRadius: 15,
  },
  profile: {
    fontSize: 24,
    padding: 3,
  },
  profile1: {
    fontSize: 24,
    marginLeft: -8,
    backgroundColor: "white",
    borderRadius: "50%",
    padding: 3,
  },
  profile2: {
    marginLeft: -8,
    color: "white",
    backgroundColor: "#5B4F48",
    width: "1.4rem",
    height: "1.4rem",
    marginTop: 2,
  },

  parent: {
    borderTop: "1px dashed #d7dadb",
    position: "relative",
    marginTop: 4,
  },
  child: {
    position: "absolute",

    top: -14,

    marginLeft: "12rem",
  },
  childcontent: {
    border: "1px solid #d7dadb",
    backgroundColor: "white",
    borderRadius: 15,
    fontSize: 12,
    padding: 6,
  },
  name: {
    fontSize: 12,
  },
  time: {
    fontSize: 11,
    color: "#A6A6A6",
  },

  msg: {
    [theme.breakpoints.up("xs")]: {
      fontSize: 12,
      fontWeight: 300,
      backgroundColor: "#f2f2f2",
      borderRadius: 15,
      padding: 10,
      width: "auto",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: 12,
      fontWeight: 300,
      backgroundColor: "#f2f2f2",
      borderRadius: 15,
      padding: 10,
      width: 400,
    },
  },
  msg1: {
    fontSize: 12,
    fontWeight: 300,
    backgroundColor: "#f2f2f2",
    borderRadius: 15,
    padding: 10,
  },
  icon: {
    fontSize: 16,
    color: "#909191",
    marginLeft: 4,
    marginTop: 4,
  },
  icon1: {
    fontSize: 20,
    color: "#909191",
    marginLeft: 4,
    cursor: "pointer",
  },
  border: {
    borderRight: "1px solid #909191",
    height: 14,
    marginTop: 4,
  },
  type: {
    border: "1px solid  #909191",
    borderRadius: 7,
  },
  input: {
    width: "100%",
    outline: "none",
    border: "none",
    backgroundColor: "transparent",
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
      marginTop: 6,
    },
    subtitle1: {
      fontSize: 15,
      fontWeight: "560",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      color: "black",
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 12,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 13,
      color: "#686868",
      fontWeight: "450",
    },
  },
});

export default function Cards() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={CardTheme}>
      <div className={classes.root}>
        <Grid container style={{ padding: 8 }}>
          <Grid item xs={1}>
            <BiConversation className={classes.icon} />
          </Grid>
          <Grid item xs={6} md={9}>
            <span>Conversation</span>
          </Grid>
          <Grid item xs={5} md={2}>
            <Box display="flex" flexDirection="row" style={{ float: "right" }}>
              <Box borderRadius="50%">
                <BsPeopleCircle className={classes.profile} />
              </Box>
              <Box>
                <BsPeopleCircle className={classes.profile1} />
              </Box>
              <Box style={{ marginLeft: -6 }}>
                <Avatar
                  className={classes.profile2}
                  style={{
                    fontSize: 11,
                    textAlign: "center",
                    marginLeft: -2,
                    marginTop: 2,
                    fontWeight: 500,
                    backgroundColor: "#535353",
                  }}
                  color="white"
                >
                  +2
                </Avatar>
              </Box>
            </Box>{" "}
          </Grid>
        </Grid>
        <Divider />
        <br />
        <div className={classes.parent}>
          <div className={classes.child}>
            <div className={classes.childcontent}>
              <span>Yesterday</span>
            </div>
          </div>
        </div>
        <Grid container>
          <Grid item xs={12}>
            <Chat
              name="Sarveswara"
              time="12.00PM"
              profile="https://www.diethelmtravel.com/wp-content/uploads/2016/04/bill-gates-wealthiest-person-279x300.jpg"
              msg="Start with the design work.Forget to update.I'll update
           
            the base of productivity framework screen once
           
            it's in good shape"
            />
          </Grid>
          <Grid item xs={12}>
            <Chat
              name="Ramya"
              time="12.02PM"
              profile="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQcJ8L5NKdzNofXm2h7NkUc-vUS8xn3F9wJLA&usqp=CAU"
              msg="Thanks for the update.we are looking forward"
            />
          </Grid>
          <Grid item xs={12}>
            <Chat
              name="Srija"
              time="12.04PM"
              profile="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQzZXXW9F6k-T7u2ZF5bmHQlCmgm0xTAez-tA&usqp=CAU"
              msg="ok"
            />
          </Grid>
        </Grid>
        <br />
        <div className={classes.parent}>
          <div className={classes.child} style={{ left: 20 }}>
            <div className={classes.childcontent}>
              <span>Today</span>
            </div>
          </div>
        </div>
        <Grid container>
          <Grid item xs={12}>
            <Chat
              name="Sarveswara"
              time="12.00PM"
              profile="https://www.diethelmtravel.com/wp-content/uploads/2016/04/bill-gates-wealthiest-person-279x300.jpg"
              msg="
              Here is the updated productivity framework screen
              design with prototype.thanks
            "
              link=" https://xd.adobe.com/view/c766a31f-62c4-4474-aea3-4c32bc2cb9bf-9235/screen/44fca6ab-a88a-4515-81f3-65eff1191790/?fullscreen              
              "
            />
            <br />
            <br />
          </Grid>
        </Grid>
        {/* messagetype */}
        <div>
          <Grid container className={classes.type}>
            <Grid
              item
              xs={12}
              style={{
                borderBottom: "1px solid  #909191",
                padding: 2,
                borderTopLeftRadius: 10,
                borderTopRightRadius: 10,
                backgroundColor: "#F2F2F2",
              }}
            >
              <Box display="flex">
                <Box className={classes.icon}>
                  <AiOutlineBold />
                </Box>
                <Box className={classes.icon}>
                  <AiOutlineItalic />
                </Box>
                <Box className={classes.icon}>
                  <AiOutlineUnderline />
                  &nbsp;&nbsp;
                </Box>
                <Box className={classes.border}></Box>
                <Box className={classes.icon}>
                  &nbsp;&nbsp;
                  <AiOutlineUnorderedList />
                </Box>
                <Box className={classes.icon}>
                  <AiOutlineOrderedList />
                </Box>
              </Box>
            </Grid>
            <Grid item xs={12}>
              <Box display="flex" flexDirection="row" p={1}>
                <Box flexGrow={1}>
                  <input
                    type="text"
                    placeholder="Type in your message..."
                    className={classes.input}
                  />
                </Box>
                <Box>
                  <IoIosAttach className={classes.icon1} />
                </Box>
                <Box>
                  <AiOutlineSmile className={classes.icon1} />
                </Box>
                <Box>
                  <AiOutlineSend className={classes.icon1} />
                </Box>
              </Box>
            </Grid>
          </Grid>
        </div>
      </div>
    </ThemeProvider>
  );
}

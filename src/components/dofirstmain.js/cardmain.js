import { Typography } from "@material-ui/core";
import Avatar from "@material-ui/core/Avatar";
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import React from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import {
  AiFillStar,
  AiOutlineCaretDown,
  AiOutlinePlusCircle,
} from "react-icons/ai";
import { BsPeopleCircle } from "react-icons/bs";
import { FiMoreVertical } from "react-icons/fi";
import { RiFlag2Fill, RiTimerLine } from "react-icons/ri";
import { VscCalendar } from "react-icons/vsc";
const useStyles = makeStyles((theme) => ({
  star: {
    fontSize: 18,
    color: "#EF9C14",
    marginTop: 2,
  },
  more: {
    fontSize: 20,
    marginTop: 2,
  },
  margin1: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
    marginLeft: 7,
  },
  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginTop: 3,
    marginLeft: 3,
    width: 50,
    float: "right",
    [theme.breakpoints.up("")]: {
      float: "left",
    },
    [theme.breakpoints.up("md")]: {
      float: "right",
    },
  },
  calander: {
    marginLeft: 3,
    marginTop: -4,
    backgroundColor: "#F1F1F1",
    width: 30,
    height: 30,
  },
  border: {
    borderRight: "1px solid #686868",
  },
  over: {
    padding: 4,
    marginTop: 1,
    borderRadius: 6,
    color: "#EFEFEF",
    cursor: "pointer",
  },
  sub: {
    color: "#E4513E",
    bgcolor: "#FCF1F0",
    border: "1px solid #E5513C",
    fontSize: 11,
    marginTop: -2,
    height: 24,
    textTransform: "capitalize",
    backgroundColor: "#FCF1F0",
  },
  dotted: {
    borderTop: "1px dashed #d7dadb",
    marginTop: 6,
    borderBottom: "1px dashed #d7dadb",
  },
  top: {
    marginTop: 14,
  },
  flagbtn: {
    backgroundColor: "#FCF1F0",
    width: 25,
    height: 25,
    marginTop: 13,
  },
  flag: {
    fontSize: 16,
  },
  profile: {
    fontSize: 24,
    padding: 3,
  },
  profile1: {
    fontSize: 24,
    marginLeft: -8,
    backgroundColor: "white",
    borderRadius: "50%",
    padding: 3,
  },
  profile2: {
    color: "white",
    backgroundColor: "#5B4F48",
    width: "1.4rem",
    height: "1.4rem",
    marginTop: 2,
  },
  btn1: {
    border: "1px solid #c9c7c7",
    textTransform: "lowercase",
    marginTop: 10,
    height: 25,
  },
  plus: {
    fontSize: 24,

    marginLeft: 4,
  },
  updateborder: {
    borderLeft: "3px solid #F5CBAD",
    marginLeft: 10,
  },
  by: {
    fontSize: 10,
    fontWeight: 400,
    marginLeft: 8,
  },
  addnew: {
    fontSize: 13,
    color: "#66BEAD",
  },
  content: {
    fontSize: 12,
    fontWeight: 440,
    marginTop: 8,
  },
  bold: {
    marginTop: 2,
  },

  update: {
    [theme.breakpoints.up("xs")]: {
      padding: 0,
    },
    [theme.breakpoints.up("md")]: {
      padding: 0,
    },
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
      marginTop: 6,
    },
    subtitle1: {
      fontSize: 15,
      fontWeight: "560",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      color: "#535353",
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 12,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 13,
      color: "#686868",
      fontWeight: "450",
    },
  },
});

export default function Cards() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={CardTheme}>
      <Box
        p={2}
        border={1}
        borderColor="#D7DADB"
        borderRadius={10}
        bgcolor="white"
      >
        <Grid container>
          <Grid item xs={10} sm={11}>
            <Typography variant="subtitle1" align="left" noWrap>
              Completion of productivity framework design on
              <br />
              before july 7th,2021
            </Typography>
          </Grid>
          <Grid item xs={2} sm={1}>
            <Box display="flex" flexDirection="row">
              <Box>
                <IconButton size="small" className={classes.star}>
                  <AiFillStar />
                </IconButton>
              </Box>
              <Box>
                <IconButton size="small" className={classes.more}>
                  <FiMoreVertical />
                </IconButton>
              </Box>
            </Box>
          </Grid>
        </Grid>

        <Grid container>
          <Grid item xs={12} sm={9}>
            <Typography variant="subtitle2" align="left" noWrap>
              Preparing to launch the Colabo on Sep 17th,2021
            </Typography>
            <Box display="flex" m={1} flexWrap="nowrap">
              <Box>
                <Button size="small" className={classes.margin}>
                  <Typography variant="body2">5157-Feature</Typography>
                </Button>
              </Box>
              <Box>
                <Button size="small" className={classes.margin1}>
                  <Typography variant="body2">Goal</Typography>
                </Button>
              </Box>
              <Box>
                <Button size="small" className={classes.margin1}>
                  <Typography variant="body2">#Colabo</Typography>
                </Button>
              </Box>
            </Box>
          </Grid>
          <Grid item xs={12} sm={3}>
            <div className={classes.progress}>
              <CircularProgressbarWithChildren
                value={35}
                strokeWidth={8}
                styles={{
                  path: {
                    stroke: `#77CF71`,
                    strokeLinecap: "butt",
                  },
                  trail: {
                    stroke: "#E4EAEE",
                    strokeLinecap: "butt",
                  },
                }}
              >
                <Typography variant="body2">35%</Typography>
              </CircularProgressbarWithChildren>
            </div>
          </Grid>
        </Grid>

        <div className={classes.dotted}>
          <br />
          <Grid container>
            <Grid item xs={1} md={1}>
              <IconButton size="small" className={classes.calander}>
                <VscCalendar />
              </IconButton>
            </Grid>
            <Grid item xs={12} sm={9} md={9}>
              <Grid container className={classes.tops}>
                <Grid item xs={5}>
                  <Box display="flex" flexDirection="row">
                    <Box>
                      <Typography variant="h5">
                        &nbsp;Jun&nbsp;29,2021
                      </Typography>
                    </Box>
                    <Box>
                      <span className={classes.border}>&nbsp;</span>
                    </Box>
                    <Box>
                      <Typography variant="h5">&nbsp;&nbsp;8.00PM</Typography>
                    </Box>
                  </Box>
                </Grid>
                <Grid item xs={false} md={1}>
                  <center>
                    <span>&nbsp;&nbsp;-&nbsp;&nbsp;&nbsp;&nbsp;</span>
                  </center>
                </Grid>
                <Grid item xs={5}>
                  <Box display="flex" flexDirection="row">
                    <Box>
                      <Typography variant="h5">
                        &nbsp;Jun&nbsp;30,2021
                      </Typography>
                    </Box>
                    <Box>
                      <span className={classes.border}>&nbsp;</span>
                    </Box>
                    <Box>
                      <Typography variant="h5">&nbsp;&nbsp;8.00PM</Typography>
                    </Box>
                  </Box>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} sm={1} md={1}>
              <Button size="small" className={classes.sub}>
                overdue
              </Button>
            </Grid>
          </Grid>
          <br />
        </div>

        <Grid container>
          <Grid item xs={1} md={1}>
            <IconButton className={classes.flagbtn} size="small">
              <RiFlag2Fill className={classes.flag} color="red" />
            </IconButton>
          </Grid>
          <Grid item xs={8} md={9}>
            <Box display="flex" flexDirection="row" marginTop={2}>
              <Box borderRadius="50%">
                <BsPeopleCircle className={classes.profile} />
              </Box>
              <Box>
                <BsPeopleCircle className={classes.profile1} />
              </Box>
              <Box style={{ marginLeft: -6 }}>
                <Avatar
                  className={classes.profile2}
                  style={{
                    fontSize: 11,
                    textAlign: "center",
                    backgroundColor: "#535353",
                    marginLeft: -2,
                    marginTop: 2,
                    fontWeight: 500,
                  }}
                  color="white"
                >
                  +2
                </Avatar>
              </Box>
              <Box>
                <AiOutlinePlusCircle className={classes.plus} />
              </Box>
              <Box marginLeft={1}>
                <RiTimerLine
                  style={{ fontSize: 20, marginTop: 2, marginLeft: 6 }}
                />
              </Box>
              <Box>
                <Typography variant="subtitle2" style={{ marginTop: 2 }}>
                  &nbsp;Est.2h 30m
                </Typography>
              </Box>
            </Box>{" "}
          </Grid>
          <Grid item xs={3} md={2}>
            <Button size="small" className={classes.btn1}>
              Pending&nbsp;
              <AiOutlineCaretDown />
            </Button>
          </Grid>
        </Grid>
      </Box>
      <br />

      {/* Recent Update */}

      <div
        style={{
          backgroundColor: "#FEF8E6",
          borderRadius: 10,
          marginTop: 4,
          padding: 9,
        }}
        className={classes.update}
      >
        <div className={classes.updateborder} style={{ padding: 2 }}>
          <div style={{ marginLeft: 2 }}>
            <Grid container>
              <Grid item xs={10} sm={10} md={10}>
                <span className={classes.bold}>Recent&nbsp;Update</span>{" "}
                <span className={classes.by}>
                  by&nbsp;sarvaswara&nbsp;at&nbsp;8.00AM
                </span>
              </Grid>

              <Grid item xs={2} sm={2} md={2}>
                <center>
                  <span className={classes.addnew}>ADD NEW</span>
                </center>
              </Grid>
              <Grid item xs={12}>
                <p className={classes.content} style={{ marginTop: 10 }}>
                  I'va completed the base UI fro focus made in Colabo.Will
                  Update will soon
                </p>
              </Grid>
            </Grid>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}

import Box from "@material-ui/core/Box";
import { createTheme, makeStyles } from "@material-ui/core/styles";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "white",
    marginLeft: 6,
    padding: 14,
    borderRadius: 15,
  },
  profile: {
    fontSize: 24,
    padding: 3,
  },
  profile1: {
    fontSize: 24,
    marginLeft: -8,
    backgroundColor: "white",
    borderRadius: "50%",
    padding: 3,
  },
  profile2: {
    marginLeft: -8,
    color: "white",
    backgroundColor: "#5B4F48",
    width: "1.4rem",
    height: "1.4rem",
    marginTop: 2,
  },

  parent: {
    borderTop: "1px dashed #d7dadb",
    position: "relative",
    marginTop: 4,
  },
  child: {
    position: "absolute",

    top: -14,

    marginLeft: "12rem",
  },
  childcontent: {
    border: "1px solid #d7dadb",
    backgroundColor: "white",
    borderRadius: 15,
    fontSize: 12,
    padding: 6,
  },
  name: {
    fontSize: 12,
  },
  time: {
    fontSize: 11,
    color: "#A6A6A6",
  },

  msg: {
    [theme.breakpoints.up("xs")]: {
      fontSize: 12,
      fontWeight: 300,
      backgroundColor: "#f2f2f2",
      borderRadius: 15,
      padding: 10,
      width: "auto",
    },
    [theme.breakpoints.up("lg")]: {
      fontSize: 12,
      fontWeight: 300,
      backgroundColor: "#f2f2f2",
      borderRadius: 15,
      padding: 8,
    },
  },
  msg1: {
    fontSize: 12,
    fontWeight: 300,
    backgroundColor: "#f2f2f2",
    borderRadius: 15,
    padding: 10,
  },
  icon: {
    fontSize: 16,
    color: "#909191",
    marginLeft: 4,
    marginTop: 4,
  },
  icon1: {
    fontSize: 20,
    color: "#909191",
    marginLeft: 4,
    cursor: "pointer",
  },
  border: {
    borderRight: "1px solid #909191",
    height: 14,
    marginTop: 4,
  },
  type: {
    border: "1px solid  #909191",
    borderRadius: 7,
  },
  input: {
    width: "100%",
    outline: "none",
    border: "none",
    backgroundColor: "transparent",
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
      marginTop: 6,
    },
    subtitle1: {
      fontSize: 15,
      fontWeight: "560",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      color: "black",
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 12,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 13,
      color: "#686868",
      fontWeight: "450",
    },
  },
});

export default function Cards(props) {
  const classes = useStyles();
  return (
    <>
      <Box display="flex" flexDirection="row" marginTop={3}>
        <Box borderRadius={10}>
          <img
            height="30"
            width="30"
            style={{ borderRadius: "50%" }}
            src={props.profile}
            alt="img"
          />
        </Box>
        <Box marginTop={1} marginLeft={1}>
          <span className={classes.name}>{props.name}</span>
        </Box>
        <Box marginTop={1} marginLeft={1}>
          <span className={classes.time}>{props.time}</span>
        </Box>
      </Box>
      <Box display="flex" flexDirection="row">
        <Box width={40}>&nbsp;</Box>
        <Box marginTop={1} marginLeft={1}>
          <div className={classes.msg}>
            <p>{props.msg} </p>
            <span style={{ textDecoration: "underline", color: "#50A5FC" }}>
              {props.link}
            </span>
          </div>
        </Box>
      </Box>
    </>
  );
}

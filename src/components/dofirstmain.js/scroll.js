import { Divider } from "@material-ui/core";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
import { BsBullseye } from "react-icons/bs";
import Actions from "./Action";
import Heading1 from "./header";
import Card from "./scrollcard";
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#F5F7F8",

    borderRadius: 14,
  },
  pad: {
    padding: 14,
  },
  box1: {
    cursor: "pointer",
    textAlign: "center",
    backgroundColor: "white",
  },
  box: {
    cursor: "pointer",
    textAlign: "center",
  },
  typo: {
    color: "#63B9A8",
    fontSize: 12,
  },
  bull: {
    fontSize: 16,
    marginTop: 2,
  },
  circle: {
    backgroundColor: "black",
    width: "1.1rem",
    height: "1.1rem",
  },
  color: {
    color: "white",
    marginLeft: 2,
  },
  typo1: {
    fontSize: 14,
  },
  para: {
    color: "#AAA9AA",
    fontSize: 13,
  },
  para1: {
    color: "#E55648",
    fontSize: 13,
    textAlign: "center",
    borderTop: "1px dashed #d7dadb",
    padding: 4,
  },
  scroll: {
    overflowY: "auto",
    margin: 0,
    padding: 0,
    scrollbarWidth: "thin",
    scrollbarColor: "orange",
  },
  dotted: {
    borderTop: "1px dashed #d7dadb",
    marginTop: 4,
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
    },
    subtitle1: {
      fontSize: 11,
      fontWeight: "bold",
    },
    body2: {
      fontSize: 14,

      color: "black",
    },
    h4: {
      fontSize: 12,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 13,
      color: "#686868",
      fontWeight: "450",
    },
  },
});

export default function Cards() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={CardTheme}>
      <div className={classes.root}>
        <Grid container className={classes.pad} spacing={1}>
          <Grid item xs={4} sm={3} md={2} lg={2}>
            <Box
              className={classes.box}
              border={1}
              p={1}
              borderRadius={20}
              bgcolor="#63B9A8"
              color="white"
            >
              info
            </Box>
          </Grid>
          <Grid item xs={4} sm={3} md={2} lg={2}>
            <Box
              className={classes.box1}
              border={1}
              borderColor="#d7dadb"
              p={1}
              borderRadius={20}
            >
              Tasks
            </Box>
          </Grid>
          <Grid item xs={4} sm={3} md={3} lg={3}>
            <Box
              className={classes.box1}
              border={1}
              p={1}
              borderRadius={20}
              borderColor="#d7dadb"
            >
              Attachment
            </Box>
          </Grid>
          <Grid item xs={4} sm={3} md={2} lg={2}>
            <Box
              className={classes.box1}
              border={1}
              p={1}
              borderRadius={20}
              borderColor="#d7dadb"
            >
              Notes
            </Box>
          </Grid>
          <Grid item xs={4} sm={3} md={3} lg={3}>
            <Box
              className={classes.box1}
              border={1}
              p={1}
              borderRadius={20}
              borderColor="#d7dadb"
            >
              Activity&nbsp;log
            </Box>
          </Grid>
        </Grid>

        <Divider style={{ marginTop: 6 }} />
        {/* scroll 1*/}
        <div style={{ padding: 6 }}>
          <Box display="flex" flexDirection="row">
            <Box flexGrow={1} m={1}>
              <Box display="flex" flexDirection="row">
                <Box>
                  <BsBullseye className={classes.bull} />
                </Box>
                <Box>
                  <span>&nbsp;&nbsp;Goals&nbsp;&nbsp;</span>
                </Box>
                <Box borderRadius="50%" className={classes.circle}>
                  <Typography variant="subtitle1" className={classes.color}>
                    02
                  </Typography>
                </Box>
              </Box>
            </Box>
            <Box>
              <p className={classes.typo}>ATTACH GOAL&nbsp;&nbsp;</p>
            </Box>
          </Box>
          {/* scroll bar*/}
          <div className={classes.scroll}>
            <Box display="flex" flexDirection="row" width={730}>
              <Box>
                <Card
                  content=" Checklist of items to be done so that weunderstand
                  what all responsibility to take forward"
                  title="Intial Launch"
                  btn="Detach"
                />
              </Box>
              <Box>&nbsp;</Box>
              <Box>
                <Card
                  content=" Checklist of items to be done so that weunderstand
                  what all responsibility to take forward"
                  title="Contect Calendar"
                  btn="Detach"
                />
              </Box>
            </Box>
          </div>
          <br />
          {/* nestde and risk */}
          <div>
            <Heading1 title="Nested Actions" number="02" />
            <Box
              border={1}
              borderRadius={15}
              borderColor="#D7DADB"
              bgcolor="white"
            >
              <Actions
                content=" Understand the base requirments and idea.."
                btncontent="Feature"
                percent="35"
              />
              <div className={classes.dotted}>
                <Actions
                  content="Prepare the design system and complete the ba.."
                  btncontent="Feature"
                  percent="70"
                  btn="true"
                />
              </div>
            </Box>
            <br />
            <Heading1 title="Risks" icon="risk" number="02" />
            <Box
              bgcolor="white"
              borderRadius={14}
              borderColor="#d7dadb"
              border={1}
            >
              <Actions
                content="working on other product design parallelly"
                btncontent="Risk"
                percent="70"
                show="false"
              />
            </Box>
          </div>
        </div>
      </div>
    </ThemeProvider>
  );
}

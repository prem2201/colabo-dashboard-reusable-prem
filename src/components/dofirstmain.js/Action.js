import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Switch from "@material-ui/core/Switch";
import Typography from "@material-ui/core/Typography";
import React, { useEffect, useState } from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import { AiFillCaretDown } from "react-icons/ai";
import { CgProfile } from "react-icons/cg";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: 8,
  },
  over: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 2,
    textTransform: "capitalize",
    border: "1px solid #E5513C",
    backgroundColor: "#FCF1F0",
    color: "#E4513E",
  },
  left: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 2,
    textTransform: "capitalize",
    border: "1px solid #EF9B12",
    backgroundColor: "#FEF7EC",
    color: "#EFA978",
  },
  bull: {
    fontSize: 16,
    marginTop: 2,
  },
  circle: {
    backgroundColor: "black",
    width: "1.1rem",
    height: "1.1rem",
    position: "relative",
  },
  color: {
    color: "white",
    marginLeft: 2,
    fontSize: 11,
    textAlign: "center",
    position: "absolute",
    top: 2,
  },
  typo: {
    color: "#63B9A8",
    fontSize: 12,
    fontWeight: 600,
  },
  arrow: {
    width: "1.1rem",
    height: "1.1rem",
    backgroundColor: "white",
  },
  typo2: {
    fontSize: 14,
  },
  switchBase: {
    padding: 1,
    "&$checked": {
      transform: "translateX(16px)",
      color: "#EEEEEE",
      "& + $track": {
        backgroundColor: "#63B9A8",
        opacity: 1,
        border: "none",
      },
    },
    "&$focusVisible $thumb": {
      color: "#EEEEEE",
      border: "6px solid #fff",
    },
  },
  thumb: {
    width: 14,
    height: 14,
    backgroundColor: "white",
  },
  roo: {
    width: 34,
    height: 16,
    padding: 0,

    margin: theme.spacing(1),
  },
  track: {
    borderRadius: 26 / 2,
    backgroundColor: "#C6C6C6",
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
  relative: {
    position: "relative",
    zIndex: 1,
  },
  typo3: {
    fontSize: 11,
    marginTop: 8,
    color: "#9C9B9C",
  },
  base: {
    fontSize: 13,
    marginTop: 5,
    [theme.breakpoints.up("md")]: {
      whiteSpace: "nowrap",
    },
    [theme.breakpoints.up("xs")]: {
      whiteSpace: "wrap",
    },
  },
  margin1: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
    marginLeft: 7,
  },
  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
  },
  sub: {
    color: "#E4513E",
    backgroundColor: "#FCF1F0",
    border: "1px solid #E5513C",
    fontSize: 11,
    textTransform: "capitalize",
    height: 25,
  },
  profile: {
    fontSize: 22,
    color: "grey",
    marginLeft: 4,
    marginTop: 2,
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginTop: 1,
    marginLeft: 3,
    width: 20,
  },
  pending: {
    marginTop: 4,
    textTransform: "capitalize",
    border: "1px solid #D7DADB",
    height: 25,
  },
  dotted: {
    borderTop: "1px dashed #d7dadb",
    marginTop: 4,
  },
  sub1: {
    color: "#EF9B12",
    border: "1px solid #EF9B12",
    backgroundColor: "#FEF7EC",
    fontSize: 11,
    textTransform: "capitalize",
    height: 25,
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
    },
    subtitle1: {
      fontSize: 14,
      fontWeight: "450",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      color: "black",
      textTransform: "lowercase",
    },
    body1: {
      fontSize: 11,
      padding: 1,
      fontWeight: 400,
      color: "#E77266",
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 12,
      marginTop: 6,
      marginLeft: 4,
    },
    h5: {
      fontSize: 10,
      color: "#EF9B12",
      textTransform: "capitalize",
    },
  },
});

export default function Cards(props) {
  const classes = useStyles();
  const [show, setShow] = useState(false);
  const [btn, setBtn] = useState(true);
  useEffect(() => {
    if (props.show == "false") {
      setShow(true);
    } else {
      setShow(false);
    }
    if (props.btn == "true") {
      setBtn(false);
    } else {
      setBtn(true);
    }
  });
  return (
    <ThemeProvider theme={CardTheme}>
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={8} sm={9}>
            <p className={classes.base}>{props.content}</p>
          </Grid>
          <Grid item xs={4} sm={3}>
            <Box display="flex" flexDirection="row" justifyContent="flex-end">
              <Box style={{ marginTop: 4 }}>
                <span className={classes.typo3}>DEPENDENT</span>
              </Box>
              <Box>
                <Switch
                  focusVisibleClassName={classes.focusVisible}
                  disableRipple
                  classes={{
                    root: classes.roo,
                    switchBase: classes.switchBase,
                    thumb: classes.thumb,
                    track: classes.track,
                    checked: classes.checked,
                  }}
                />
              </Box>
            </Box>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item xs={12} sm={10}>
            <Box display="flex" flexWrap="nowrap">
              <Box>
                <Button size="small" className={classes.margin}>
                  <Typography
                    variant="body2"
                    style={{ textTransform: "capitalize" }}
                  >
                    5157-{props.btncontent}
                  </Typography>
                </Button>
              </Box>
              <Box>
                &nbsp;&nbsp;
                {btn ? (
                  <Button size="small" className={classes.over}>
                    overdue
                  </Button>
                ) : (
                  <Button size="small" className={classes.left}>
                    2 hours
                  </Button>
                )}
              </Box>
              <div hidden={show}>
                <Box display="flex" flexWrap="nowrap">
                  <Box>
                    <CgProfile className={classes.profile} />
                  </Box>
                  <Box>
                    <div className={classes.progress}>
                      <CircularProgressbarWithChildren
                        value={35}
                        strokeWidth={14}
                        styles={{
                          path: {
                            stroke: `#77CF71`,
                            strokeLinecap: "butt",
                          },
                          trail: {
                            stroke: "#d6d6d6",
                            strokeLinecap: "butt",
                          },
                        }}
                      ></CircularProgressbarWithChildren>{" "}
                    </div>
                  </Box>
                  <Box>
                    <Typography variant="h4">{props.percent}%</Typography>
                  </Box>
                </Box>
              </div>
            </Box>
          </Grid>
          <Grid item xs={12} sm={2}>
            <Button size="small" className={classes.pending}>
              Pending&nbsp;
              <AiFillCaretDown />
            </Button>
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
}

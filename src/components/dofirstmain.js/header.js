import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import React, { useEffect } from "react";
import { AiOutlineWarning } from "react-icons/ai";
import { FiGitPullRequest } from "react-icons/fi";
import { RiArrowUpSLine } from "react-icons/ri";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: 8,
  },
  over: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 2,
    textTransform: "capitalize",
    border: "1px solid #E5513C",
    backgroundColor: "#FCF1F0",
    color: "#E4513E",
  },
  left: {
    padding: 4,
    borderRadius: 4,
    height: 24,
    width: 50,
    fontSize: 11,
    marginTop: 2,
    textTransform: "capitalize",
    border: "1px solid #EF9B12",
    backgroundColor: "#FEF7EC",
    color: "#EFA978",
  },
  bull: {
    fontSize: 16,
    marginTop: 2,
  },
  circle: {
    backgroundColor: "black",
    width: "1.1rem",
    height: "1.1rem",
    position: "relative",
  },
  color: {
    color: "white",
    marginLeft: 2,
    fontSize: 11,
    textAlign: "center",
    position: "absolute",
    top: 2,
  },
  typo: {
    color: "#63B9A8",
    fontSize: 12,
    fontWeight: 600,
  },
  arrow: {
    width: "1.1rem",
    height: "1.1rem",
    backgroundColor: "white",
  },
  typo2: {
    fontSize: 14,
  },
  switchBase: {
    padding: 1,
    "&$checked": {
      transform: "translateX(16px)",
      color: "#EEEEEE",
      "& + $track": {
        backgroundColor: "#63B9A8",
        opacity: 1,
        border: "none",
      },
    },
    "&$focusVisible $thumb": {
      color: "#EEEEEE",
      border: "6px solid #fff",
    },
  },
  thumb: {
    width: 14,
    height: 14,
    backgroundColor: "white",
  },
  roo: {
    width: 34,
    height: 16,
    padding: 0,

    margin: theme.spacing(1),
  },
  track: {
    borderRadius: 26 / 2,
    backgroundColor: "#C6C6C6",
    opacity: 1,
    transition: theme.transitions.create(["background-color", "border"]),
  },
  checked: {},
  focusVisible: {},
  relative: {
    position: "relative",
    zIndex: 1,
  },
  typo3: {
    fontSize: 11,
    marginTop: 8,
    color: "#9C9B9C",
  },
  base: {
    fontSize: 13,
    marginTop: 5,
    [theme.breakpoints.up("md")]: {
      whiteSpace: "nowrap",
    },
    [theme.breakpoints.up("xs")]: {
      whiteSpace: "wrap",
    },
  },
  margin1: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
    marginLeft: 7,
  },
  margin: {
    backgroundColor: "#F1F1F1",
    color: "#676767",
  },
  sub: {
    color: "#E4513E",
    backgroundColor: "#FCF1F0",
    border: "1px solid #E5513C",
    fontSize: 11,
    textTransform: "capitalize",
    height: 25,
  },
  profile: {
    fontSize: 22,
    color: "grey",
    marginLeft: 4,
    marginTop: 2,
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginTop: 1,
    marginLeft: 3,
    width: 20,
  },
  pending: {
    marginTop: 4,
    textTransform: "capitalize",
    border: "1px solid #D7DADB",
    height: 25,
  },
  dotted: {
    borderTop: "1px dashed #d7dadb",
    marginTop: 4,
  },
  sub1: {
    color: "#EF9B12",
    border: "1px solid #EF9B12",
    backgroundColor: "#FEF7EC",
    fontSize: 11,
    textTransform: "capitalize",
    height: 25,
  },
}));

export default function Cards(props) {
  const classes = useStyles();
  useEffect(() => {
    if (props.icon == "risk") {
      setIcon(false);
    } else {
      setIcon(true);
    }
  });
  const [icon, setIcon] = React.useState(true);
  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item xs={9}>
          <Box display="flex" flexDirection="row">
            <Box>
              {icon ? (
                <FiGitPullRequest className={classes.bull} />
              ) : (
                <AiOutlineWarning className={classes.bull} />
              )}
            </Box>
            <Box>
              <span>&nbsp;&nbsp;{props.title}&nbsp;&nbsp;</span>
            </Box>
            <Box borderRadius="50%" className={classes.circle}>
              <span className={classes.color}>{props.number}</span>
            </Box>
          </Box>
        </Grid>
        <Grid item xs={3}>
          <Box display="flex" flexDirection="row">
            <Box width={40}>&nbsp;</Box>
            <Box>
              <span className={classes.typo}>ADD&nbsp;NEW&nbsp;&nbsp;</span>
            </Box>
            <Box
              borderRadius="50%"
              className={classes.arrow}
              borderColor="#D7DADB"
              border={1}
            >
              <RiArrowUpSLine style={{ fontSize: 14 }} />
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}

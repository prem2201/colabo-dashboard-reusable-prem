import Box from "@material-ui/core/Box";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import "react-circular-progressbar/dist/styles.css";
const useStyles = makeStyles((theme) => ({
  typo1: {
    fontSize: 14,
  },
  para: {
    color: "#AAA9AA",
    fontSize: 13,
  },
  para1: {
    color: "#E55648",
    fontSize: 13,
    textAlign: "center",
    borderTop: "1px dashed #d7dadb",
    padding: 4,
  },
}));

export default function Cards(props) {
  const classes = useStyles();
  return (
    <>
      <Box
        bgcolor="white"
        p={1}
        borderRadius={14}
        borderColor="#d7dadb"
        border={1}
      >
        <span className={classes.typo1}>{props.title}</span>
        <p className={classes.para}>{props.content}</p>
        <div className={classes.para1}>
          <span style={{ marginTop: 4 }}>{props.btn}</span>
        </div>
      </Box>
    </>
  );
}

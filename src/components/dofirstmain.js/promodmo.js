import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import {
  createTheme,
  makeStyles,
  ThemeProvider,
} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import React from "react";
import { CircularProgressbarWithChildren } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";
import { AiOutlinePauseCircle, AiOutlineSetting } from "react-icons/ai";
import { BiReset } from "react-icons/bi";
import { GiSandsOfTime } from "react-icons/gi";
import { IoIosArrowDroprightCircle } from "react-icons/io";
import { MdTimer } from "react-icons/md";
const useStyles = makeStyles((theme) => ({
  root: {
    padding: 4,
  },
  progress: {
    color: "#77CF71",
    padding: 1,
    marginTop: 12,
    marginLeft: 3,
    height: 150,
    width: 150,
    boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 #FDF3E2",
  },
  focus: {
    fontSize: 12,
    color: "#9D9D9D",
  },
  time: {
    fontSize: 11,
    color: "#8D8D8D",
  },
  min: {
    fontSize: 12,
    marginTop: -3,
  },
  bgimg: {
    backgroundImage: `url(${"https://image.freepik.com/free-vector/flat-hand-drawn-people-analyzing-growth-chart-illustration_23-2148860943.jpg"})`,
    height: 150,
    backgroundSize: "cover",
    borderRadius: 10,
    border: "1px solid #2694FD",
  },
  bgimg1: {
    backgroundImage: `url(${"https://image.freepik.com/free-vector/business-idea-concept-with-people_52683-28609.jpg"})`,
    height: 150,
    backgroundSize: "cover",
    borderRadius: 10,
    border: "1px solid #D7DADB",
  },

  work: {
    fontWeight: 600,
    marginLeft: 8,
    fontSize: 14,
    padding: 8,
  },
  border: {
    borderTop: "1px dashed #d7dadb",
  },
}));
const CardTheme = createTheme({
  palette: {
    secondary: {
      main: "#F2F4F6",
    },
  },
  typography: {
    subtitle2: {
      fontSize: 13,
      color: "#585858",
      wordSpacing: 1,
      fontWeight: "450",
      marginTop: 6,
    },
    subtitle1: {
      fontSize: 15,
      fontWeight: "560",
      fontFamily: "adobe-clean, sans-serif",
    },
    body2: {
      fontSize: 11,
      padding: 1,
      color: "black",
      textTransform: "lowercase",
    },
    h4: {
      fontSize: 12,
      fontWeight: "bold",
    },
    h5: {
      fontSize: 13,
      color: "#686868",
      fontWeight: "450",
    },
  },
});

export default function Cards() {
  const classes = useStyles();
  return (
    <ThemeProvider theme={CardTheme}>
      <div className={classes.root}>
        <Grid conteiner>
          <Grid item xs={12} md={12} lg={12}>
            <Box
              p={2}
              borderRadius={10}
              bgcolor="white"
              className={classes.shadow}
            >
              <Grid container>
                <Grid item xs={11}>
                  <span style={{ fontWeight: 550 }}>Pomodoro</span>
                </Grid>
                <Grid item xs={1}>
                  <AiOutlineSetting style={{ fontSize: 18 }} />
                </Grid>
              </Grid>
              <center>
                <Box className={classes.progress} borderRadius="50%">
                  <CircularProgressbarWithChildren
                    value={35}
                    strokeWidth={20}
                    styles={{
                      path: {
                        stroke: `#EF9A12`,
                        strokeLinecap: "butt",
                      },
                      trail: {
                        stroke: "#F2F4F6",
                        strokeLinecap: "butt",
                      },
                    }}
                  >
                    <span
                      style={{
                        fontWeight: "bold",
                        fontSize: 18,
                        color: "black",
                      }}
                    >
                      12:00
                    </span>
                  </CircularProgressbarWithChildren>
                </Box>
                <br />
                <span className={classes.focus}>Stay focus for 20 minutes</span>
                <br />
                <Box
                  display="flex"
                  flexDirection="row"
                  justifyContent="center"
                  m={2}
                >
                  <Box
                    bgcolor="#63B9A8"
                    color="white"
                    borderRadius={8}
                    style={{ padding: 8 }}
                    height={44}
                    width={44}
                  >
                    <AiOutlinePauseCircle style={{ fontSize: 26 }} />
                  </Box>
                  <Box>&nbsp;&nbsp;</Box>
                  <Box
                    color="#63B9A8"
                    borderRadius={8}
                    border={1}
                    borderColor="#63B9A8"
                    style={{ padding: 8 }}
                    height={44}
                    width={44}
                  >
                    <BiReset style={{ fontSize: 26, color: "#63B9A8" }} />
                  </Box>
                </Box>
              </center>
              <div className={classes.border}>
                <br />
                <Grid container>
                  <Grid item xs={6}>
                    <Box
                      display="flex"
                      flexDirection="row"
                      justifyContent="center"
                    >
                      <Box
                        borderRadius="50%"
                        bgcolor="#FEF7EC"
                        style={{
                          height: "1.5rem",
                          width: "1.5rem",
                          marginTop: 10,
                        }}
                      >
                        <MdTimer
                          style={{
                            fontSize: 20,
                            color: "#F4BA5A",
                            marginTop: 2,
                            marginLeft: 2,
                          }}
                        />
                      </Box>
                      <Box marginLeft={1}>
                        <span className={classes.time}>Est. time</span>
                        <br />
                        <p className={classes.min}>2h 30m</p>
                      </Box>
                    </Box>
                  </Grid>
                  <Grid item xs={6}>
                    <Box display="flex" flexDirection="row">
                      <Box
                        borderRadius="50%"
                        bgcolor="#FEF7EC"
                        style={{
                          height: "1.5rem",
                          width: "1.5rem",
                          marginTop: 10,
                        }}
                      >
                        <GiSandsOfTime
                          style={{
                            fontSize: 20,
                            color: "#F4BA5A",
                            marginTop: 2,
                            marginLeft: 2,
                          }}
                        />
                      </Box>
                      <Box marginLeft={1}>
                        <span className={classes.time}>spend&nbsp;time</span>
                        <br />
                        <p className={classes.min}>1h 15m</p>
                      </Box>
                    </Box>
                  </Grid>
                </Grid>
              </div>
            </Box>
            <br />
          </Grid>
          <Grid item xs={12} md={12} lg={12}>
            <Box p={2} borderRadius={10} bgcolor="white">
              <Grid container>
                <Grid item xs={12}>
                  <div className={classes.bgimg}>
                    <Typography className={classes.work}>
                      I've completed <br />
                      work
                    </Typography>
                    <br />
                    <br />

                    <IoIosArrowDroprightCircle
                      style={{ fontSize: 24, marginLeft: 8, marginTop: 8 }}
                    />
                  </div>
                </Grid>

                <Grid item xs={12}>
                  <br />

                  <div className={classes.bgimg1}>
                    <Typography className={classes.work}>
                      I will come back <br />
                      later
                    </Typography>
                    <br />
                    <br />

                    <IoIosArrowDroprightCircle
                      style={{ fontSize: 24, marginLeft: 8, marginTop: 8 }}
                    />
                  </div>
                </Grid>
              </Grid>
            </Box>
          </Grid>
        </Grid>
      </div>
    </ThemeProvider>
  );
}

import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import React from "react";
import Card from "./cardmain";
import Conversion from "./conversion";
import Promodo from "./promodmo";
import Scroll from "./scroll";

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: "#f2f2f2",
    overflow: "hidden",
  },
  pad: {
    padding: 10,
    height: 830,
    overflow: "auto",
    width: "100%",
  },
}));

export default function Main() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={1}>
        <Grid
          item
          xs={12}
          sm={12}
          md={6}
          lg={5}
          style={{ backgroundColor: "white", borderRadius: 15 }}
        >
          <Grid container>
            <Grid item xs={false} sm={false} md={false} lg={1}></Grid>
            <Grid item xs={12} sm={12} md={12} lg={11}>
              <div className={classes.pad}>
                <Card />
                <br />
                <Scroll />
              </div>
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={4}>
          <div className={classes.conversion}>
            <Conversion />
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={6} lg={3}>
          <Box display="flex" flexDirection="row">
            <Box width="90%">
              <Promodo />
            </Box>
            <Box width="10%" bgcolor="white">
              &nbsp;
            </Box>
          </Box>
        </Grid>
      </Grid>
    </div>
  );
}

import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Main from "../components/dofirstmain.js/index";
import { Home, NotFound } from "./../screens";
import { Routes } from "./routes";
const RouterApp = (props) => {
  return (
    <Router>
      <Switch>
        {/* Login Route */}
        <Route exact path={Routes.home}>
          <Home />
        </Route>

        <Route exact path={Routes.goals}>
          <Main />
        </Route>

        {/* Home Route */}
        {/* <PrivateRouter exact path={Routes.home}>
          <Home />
        </PrivateRouter> */}

        {/* For unknow/non-defined path */}
        <Route exact path="*" component={NotFound} />
      </Switch>
    </Router>
  );
};

export default RouterApp;
